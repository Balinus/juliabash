# Quantile-Quantile bias correction from the shell

# Table des matières (FR)
1. [Aide](#help_en)
2. [Usage](#usage_fr)  
3. [Installation](#installation_fr)
4. [Exemple](#example_fr)

# Table of content (Eng)
[English](#english)
1. [Help](#help_en)
2. [Usage](#usage_en)  
3. [Installation](#installation_en)
4. [Example](#example_en)

## Help <a name="help_en"></a>

Lancer `julia biascorrect.jl -h` pour de l'aide.
Launch `julia biascorrect.jl -h` for help.

```bash
$ julia biascorrect.jl --help

usage: biascorrect.jl [--output OUTPUT] [--shapefile SHAPEFILE]
                      [--shapefile_part SHAPEFILE_PART]
                      [--detrend DETREND] [--regrid REGRID]
                      [--method METHOD] [--rank RANK]
                      [--verbose VERBOSE] [-h] obs ref fut var

positional arguments:
  obs                   Observation Dataset (netCDF file or directory)
  ref                   Dataset that will be compared with Observed
                        Dataset (netCDF file or directory)
  fut                   Dataset to be corrected (netCDF file or
                        directory)
  var                   Variable inside the netCDF files

optional arguments:
  --output OUTPUT       Output file (default: "output.nc")
  --shapefile SHAPEFILE
                        Shapefile for targeted spatial extration
                        (default: "")
  --shapefile_part SHAPEFILE_PART
                        Parts of the shapefile wanted (type: Int64,
                        default: 1)
  --detrend DETREND     Remove temporal trends at each grid point
                        before bias correction (type: Bool, default:
                        true)
  --regrid REGRID       Regrid ref and fut datasets onto obs grid
                        (type: Bool, default: false)
  --method METHOD       Multiplicative or Additive transfer function
                        (default: "Additive")
  --rank RANK           Number of bins to use for Quantile estimation
                        (type: Int64, default: 50)
  --verbose VERBOSE     Verbosity (type: Bool, default: false)
  -h, --help            show this help message and exit
```


## Usage <a name="usage_fr"></a>

```bash
# À partir du répertoire "src"
$ julia biascorrect.jl obsdata.nc refdata.nc futdata.nc pr --method "Multiplicative" --detrend true --rank 50 --output output.nc
```

### Arguments positionnels

* **obs.nc** ou **/chemin/des/fichiers** Dataset D'observation (fichier netCDF file ou répertoire)

* **ref.nc** or **/path/to/files** Dataset qui sera comparé auxc observations pour l'estimation des fonctions de transfert (fichier netCDF ou répertoire)

* **fut.nc** or **/path/to/files** Dataset qui sera corrigé par les fonctions de transferts (fichier netCDF ou répertoire)

* **var** Nom de la variable dans les fichiers netCDF files (par ex. pr, tasmax, tasmin, etc.)

### Arguments optionnels

* **--output** Fichier de sortie

* **--detrend** Soustraction de la tendance avant l'estimation et l'application des fonctions de transferts

* **--regrid** Interpolation de `ref` et `fut` sur la grille `obs`

* **--method** Méthode d'application des fonctions de transferts (`Multiplicative` ou `Additive`)

* **--shapefile** Shapefile `.shp` pour une extraction spatiale ciblée

* **--shapefile_part** Partie du shapefile désirée

* **--rank** Nombre de bins pour les fonctions de transferts

* **--verbose** Verbosité

*N.B. refdata.nc et futdata.nc peuvent être le même dataset.*

## Installation <a name="installation_fr"></a>

1. Dans votre fichier `.bashrc`, il est nécessaire de charger quelques modules: Julia, nombre de threads, une version spécifique de la librairie `libz.so` et un environnement Python particulier.

```bash
# .bashrc file
module load JULIA/1.X.Y # verify the latest version with "module avail"
export JULIA_NUM_THREADS=16 # allocate 16 threads for multi-threading execution
alias julia="export LD_LIBRARY_PATH=/home/proy/.julia/conda/3/lib; LD_PRELOAD=/home/proy/.julia/conda/3/lib/libz.so julia -O3" # Specific optimized version of libz.so
module load Anaconda # We need a specific python distribution
```

Rédémarrer le terminal ou simplement exécuter le fichier `.bashrc` avec `source .bashrc` et activer l'environnement Conda `python36`.

```bash
# bash (terminal)
$ source ~/.bashrc
$ source activate python36 # this can be added at the end of your .bashrc file
```

2. a) Ajouts des packages dans Julia

Pour lancer julia, simplement taper `julia` dans le terminal.

**Note.** La gestion des paquets/modules/librairie dans Julia se fait directement dans le terminal Julia. Le mode `package manager` est accessible dans julia en pressant le bracket droit `]` dans le REPL julia.

```julia
# julia -> presser "]"
pkg> add PyCall Conda Argparse ClimateTools # in pkg mode
```

2. b) Configuration de PyCall (Python est essentiellement utilisé pour faire des cartes dans ClimateTools.jl)

**Neree** and **Doris**

Dans Julia.

```julia
# julia
julia> ENV["PYTHON"]="/usr/local/DEV/anaconda3/envs/python36/bin/python"
julia> using Pkg; Pkg.build("PyCall")
pkg> test ClimateTools
```

Il peut être nécessaire de redémarrer Julia après avoir fait un build de PyCall.

## Example <a name="example_fr"></a>

Supposons la structure suivante pour un projet `project`. Les observations et la simulation de référence sont dispersées sur plusieurs fichiers. La simulation future est contenu dans un seul fichier. Nous avons également un fichier `Shapefile.shp` pour notre région d'intérêt.

```
project
│   README.md
|   Shapefile.shp
│
└───observations_folder
│   │   Observations1.nc
│   │   Observations2.nc
|   |   ...
|   |   Observations34.nc
│   
└───reference_folder
|   │   ReferenceSimulation1.nc
|   │   ReferenceSimulation2.nc
|
└───future_folder
        │   FutureSimulation.nc        
```

Pour faire la correction de biais de `FutureSimulation.nc`, il suffit de lancer la commande suivante (ici, lancée à partir de la racine du project, i.e. le répertoire `project`) :

```bash
# Lancée à partir du répertoire "project"
$ julia /home/user/gitrepos/juliabash/src/biascorrectl.jl observations_folder/ reference_folder/ future_folder/FutureSimulation.nc precipitation --method multiplicative --detrend true --regrid true --shapefile Shapefile.shp --shapefile_part 1
```

Dans la commande précédente, les options suivantes ont été utilisées :

* Le script `biascorrect.jl` est situé dans `/home/user/gitrepos/juliabash/src`.
* Le nom de la variable des fichiers netCDF est `precipitation`
* La méthode d'application des fonctions de transferts est multiplicative (`--method multiplicative`)
* La conservation de la tendance est souhaitée (`--detrend true`)
* Nous avons interpolé les simulations sur la grille d'observation (`--regrid true`)
* On a extrait et corrigé les points de grille situés à l'intérieur de la partie `1` du shapefile `Shapefile.shp` (`--shapefile Shapefile.shp --shapefile_part 1`)
* On veut voir ce qui se passe (`--verbose true`)

# English <a name="english"></a>

## Usage <a name="usage_en"></a>

```bash
# From the "src" folder
$ julia biascorrect.jl obsdata.nc refdata.nc futdata.nc pr --method "Multiplicative" --detrend true --rank 50 --output output.nc
```

### Positional arguments

* **obs.nc** or **/path/to/files** Observation Dataset (netCDF file or directory)

* **ref.nc** or **/path/to/files** Dataset that will be compared with Observed Dataset (netCDF file or directory)

* **fut.nc** or **/path/to/files** Dataset to be corrected (netCDF file or directory)

* **var** Variable inside the netCDF files (e.g. pr, tasmax, tasmin)

### Optional arguments

* **--output** Output file (default: "output.nc")

* **--detrend** Remove temporal trends at each grid point before bias correction (type: Bool, default: true)

* **--regrid** Regrid ref and fut datasets onto obs grid (type: Bool, default: true)

* **--method** Multiplicative or Additive transfer function (default: "Additive")

* **--shapefile** Shapefile `.shp` for spatial subsetting of the files

* **--shapefile_part** Part of the shapefile wanted

* **--rank** Number of bins to use for Quantile estimation (type: Int64, default: 50)

* **--verbose** Verbosity (type: Bool, default: false)

*N.B. refdata.nc and futdata.nc can be the same file.*

## Installation <a name="installation_en"></a>

1. Put in your `.bashrc` file the following lines to load Julia, number of threads and appropriate libraries.

```bash
# .bashrc file
module load JULIA/1.X.Y # verify the latest version with "module avail"
export JULIA_NUM_THREADS=16 # allocate 16 threads for multi-threading execution
alias julia="export LD_LIBRARY_PATH=/home/proy/.julia/conda/3/lib; LD_PRELOAD=/home/proy/.julia/conda/3/lib/libz.so julia -O3" # Specific optimized version of libz.so
module load Anaconda # We need a specific python distribution
```

Restart terminal or execute your `.bashrc` file with `source .bashrc` and activate `python36` conda environment.

```bash
# bash (terminal)
$ source ~/.bashrc
$ source activate python36 # this can be added at the end of your .bashrc file
```

2. a) Add dependencies in Julia

To start julia, simply type `julia` in your terminal.

**Note.** Package mode is accessed by pressing the right-bracket `]` in julia.

```julia
# julia
pkg> add PyCall Conda Argparse ClimateTools # in pkg mode
```

2. b) Configure python dependencies (Python is used for mapping purpose in ClimateTools)

**Neree** and **Doris**

```julia
# julia
julia> ENV["PYTHON"]="/usr/local/DEV/anaconda3/envs/python36/bin/python"
julia> using Pkg; Pkg.build("PyCall")
pkg> test ClimateTools
```

It might be necessary to restart `julia` after setting up and building PyCall.

## Example <a name="example_en"></a>

Say you have the following directory structure for a specific project. Your observations and reference files are contained into multiple files and your future simulation has only one file. You have a shapefile of your region of interest `Shapefile.shp`.

```
project
│   README.md
|   Shapefile.shp
│
└───observations_folder
│   │   Observations1.nc
│   │   Observations2.nc
│   
└───reference_folder
|   │   ReferenceSimulation1.nc
|   │   ReferenceSimulation2.nc
|
└───future_folder
        │   FutureSimulation.nc        
```
To bias correct `FutureSimulation.nc` with the transfer function between `ObservationsX.nc` and `ReferenceSimulationX.nc` files, the following command is needed (launched from the root folder of your project i.e. `project` folder):

```bash
# Launched from project folder
$ julia /home/user/gitrepos/juliabash/src/biascorrectl.jl observations_folder/ reference_folder/ future_folder/FutureSimulation.nc precipitation --method multiplicative --detrend true --regrid true --shapefile Shapefile.shp --shapefile_part 1
```

With the following assumptions/choices:

* The command line interface `biascorrect.jl` is located in `/home/user/gitrepos/juliabash/src`.
* Variable inside the netCDF files is `precipitation`
* The method is multiplicative (`--method multiplicative`)
* You want detrending (`--detrend true`)
* Regridding to the observations grid is needed (`--regrid true`)
* Spatial subsetting is needed (`--shapefile file.shp`)
* You want to see what's happening (`--verbose true`)
