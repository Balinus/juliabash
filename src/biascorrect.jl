using ArgParse

# ======================
# COMMAND LINE INTERFACE
# ======================
function parse_commandline()
    s = ArgParseSettings()

    @add_arg_table s begin
        "obs"
            help = "Observation Dataset (netCDF file or directory)"
            arg_type = String
            required = true
        "ref"
            help = "Dataset that will be compared with Observed Dataset (netCDF file or directory)"
            arg_type = String
            required = true
        "fut"
            help = "Dataset to be corrected (netCDF file or directory)"
            arg_type = String
            required = true
            # action = :store_true
        "var"
            help = "Variable inside the netCDF files"
            arg_type = String
            required = true
        "--output"
            help = "Output file"
            arg_type = String
            default = "output.nc"
            required = false
        "--shapefile"
            help = "Shapefile for targeted spatial extration"
            arg_type = String
            default = ""
            required = false
        "--shapefile_part"
            help = "Parts of the shapefile wanted"
            arg_type = Int
            default = 1
            required = false
        "--detrend"
            help = "Remove temporal trends at each grid point before bias correction"
            arg_type = Bool
            default = true
            required = false
        "--regrid"
            help = "Regrid ref and fut datasets onto obs grid"
            arg_type = Bool
            default = false
            required = false
        "--method"
            help = "Multiplicative or Additive transfer function"
            arg_type = String
            default = "Additive"
            required = false
        "--rank"
            help = "Number of bins to use for Quantile estimation"
            arg_type = Int
            default = 50
            required = false
        "--verbose"
            help = "Verbosity"
            arg_type = Bool
            default = false
            required = false
    end

    return parse_args(s)
end

function print_rgb(r, g, b, t)
    print("\e[1m\e[38;2;$r;$g;$b;249m",t)
end

function main(parsed_args)

    if !isempty(parsed_args["shapefile"])
        P = extractpoly(parsed_args["shapefile"], parsed_args["shapefile_part"])
    end

    # ============================
    # LOAD DATA
    # ============================
    if parsed_args["verbose"]
        println("Loading Data")
    end

    # OBSERVATION
    if isdir(parsed_args["obs"])
        obsfiles = glob("*.nc", parsed_args["obs"])
    elseif isfile(parsed_args["obs"])
        obsfiles = parsed_args["obs"]
    end
    if !isempty(parsed_args["shapefile"])
        obsdata = load(obsfiles, parsed_args["var"], poly=P)
    else
        obsdata = load(obsfiles, parsed_args["var"])
    end

    # REFERENCE (SIMULATION)
    if isdir(parsed_args["ref"])
        reffiles = glob("*.nc", parsed_args["ref"])
    elseif isfile(parsed_args["ref"])
        reffiles = parsed_args["ref"]
    end
    if !isempty(parsed_args["shapefile"])
        refdata = load(reffiles, parsed_args["var"], poly=P)
    else
        refdata = load(reffiles, parsed_args["var"])
    end

    # FUTURE (SIMULATION)
    if isdir(parsed_args["fut"])
        futfiles = glob("*.nc", parsed_args["fut"])
    elseif isfile(parsed_args["fut"])
        futfiles = parsed_args["fut"]
    end
    if !isempty(parsed_args["shapefile"])
        futdata = load(futfiles, parsed_args["var"], poly=P)
    else
        futdata = load(futfiles, parsed_args["var"])
    end

    # REGRID
    if parsed_args["regrid"]
        if parsed_args["verbose"]
            println("Regridding ref and fut file onto obs grid")
        end
        refdata = regrid(refdata, obsdata)
        futdata = regrid(futdata, obsdata)
    end

    # =================================
    # QUANTILE-QUANTILE BIAS CORRECTION
    # =================================
    if parsed_args["verbose"]
        println("Bias correction. This can be long depending on the size of your datasets")
    end
    if parsed_args["verbose"]
        t1 = time()
    end

    # Quantile-Quantile function
    qq = qqmap(obsdata, refdata, futdata, detrend=parsed_args["detrend"], method=parsed_args["method"], rankn=parsed_args["rank"])

    if parsed_args["verbose"]
        t2 = time()
    end
    if parsed_args["verbose"]
        println("==========================")
        println("BIAS CORRECTION STATISTICS")
        println(string("* Time (bias-correction): ", round(t2-t1, digits=2), "s"))
        println(string("* Grid-points: ", size(futdata[1],1)*size(futdata[1],2)))
        println(string("* Time-steps: ", size(futdata[1][1,1,:],1)))
        println("===========================")
    end

    # ============================
    # WRITE FILE
    # ============================
    if parsed_args["verbose"]
        println(string("Writing to disk => ", parsed_args["output"]))
    end
    write(qq, parsed_args["output"])

end

# ============================
# EXECUTION
# ============================

# Parse command
parsed_args = parse_commandline()

if !haskey(parsed_args, "help")

    # MAIN DEPENDENCE
    using ClimateTools

    # BASIC INFO
    if parsed_args["verbose"]
        println(string("USING ", Threads.nthreads(), " THREADS"))
        println("Arguments:")
        for (arg,val) in parsed_args
            println("  $arg  =>  $val")
        end
    end

    # LOAD GLOB IF ARGUMENTS ARE DIRECTORIES
    if isdir(parsed_args["obs"]) || isdir(parsed_args["ref"]) || isdir(parsed_args["fut"])
        using Glob
    end
    main(parsed_args)
end
